# Documentation

## Containers
The container only contains the Alpine base-image and this java program.

## Script everything
There is a script for the automatic build for this project. It is called .gitlab-cy.yml.


## Choose your framework wisely
The java spring framework is used. It is well established and much documentation can be found.

## Secure your queries
Spring can be used with ORMs. There is no need to write DAOs or sql-queries. Therefore, it is not
possible to inject any text.

## Clean code
It has been tried to keep the code simple. For all three objects (buildings, storeys, rooms)
one controller, service and repository was created. There is one additional service which is used
from all controllers for the responses. It has also been tried to make it easy to understand  
when which response will be sent. There is always one if-statement with (mainly) a simple named method.
And if the if-statement is evaluated to false it will directly log the situation and
sends the response in the else part.

## Validate the JWT
The JWT is not validated, there is only a function which can analyze a JWT Token

## Tracing
We did not do the tracing


## Configurability
Many options from the docker-compose files can be configured in the .env file.
For local configurations the application.properties file can be used.

Configurable parameters:

- Database settings
  - spring.datasource.url: The url where the database can be found
  - spring.datasource.driver-class-name: The driver for the database
  - spring.datasource.username
  - spring.datasource.password
- backend.reservation.url: The url where the reservations api can be found

In the docker compose file you can also configure the image repository and version
for the backend container

## Licensing
MIT license is used.

## Version control
Git is used. Repository can be found on [gitlab](https://gitlab.com/janick3110/Webengineering-Backend).

## Write documentation
This README.md

## Testautomation
Two java unit tests were written. These tests will be automatically executed with the
maven package command.
When the tests fail the build process will be stopped.

## Logging:
- only in files will be logged in ecs format
- console logs with default spring schema 

## API
All requests parameters will be checked before they will be used for next instructions.