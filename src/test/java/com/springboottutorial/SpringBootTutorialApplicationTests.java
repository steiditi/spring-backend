package com.springboottutorial;

import com.springboottutorial.service.MainService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.springboottutorial.model.Error;


class SpringBootTutorialApplicationTests {

    private final MainService service = new MainService();

    @Test
    public void testEmptyResponse() {
        ResponseEntity<String> entity = service.getEmptyResponseEntity(HttpStatus.OK);
        Assertions.assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test
    public void testErrorResponse() {
        Error error = new Error("Fehler");
        ResponseEntity<String> entity = service.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
        Assertions.assertEquals("{" + "\r\n" +
                "  \"message\": \"" + "Fehler" + "\",\r\n" +
                "  \"additionalProp1\": " + "{}" + "\r\n" +
                "}", entity.getBody());
    }

}
