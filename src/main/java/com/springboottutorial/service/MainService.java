package com.springboottutorial.service;

import com.springboottutorial.model.Building;
import com.springboottutorial.model.Error;
import com.springboottutorial.model.Room;
import com.springboottutorial.model.Storey;
import org.json.JSONArray;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class MainService {

    public ResponseEntity<String> getEmptyResponseEntity(HttpStatus httpStatus) {
        return new ResponseEntity<>(httpStatus);
    }

    public ResponseEntity<String> getBasicResponseEntity(Building building, HttpStatus httpStatus) {
        return getBasicResponseEntity(List.of(building), httpStatus);
    }

    public ResponseEntity<String> getBasicResponseEntity(Storey storey, HttpStatus httpStatus) {
        return getBasicResponseEntity(List.of(storey), httpStatus);
    }

    public ResponseEntity<String> getBasicResponseEntity(Room room, HttpStatus httpStatus) {
        return getBasicResponseEntity(List.of(room), httpStatus);
    }

    public ResponseEntity<String> getBasicResponseEntity(Iterable<?> buildings, HttpStatus httpStatus) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONArray array = new JSONArray(buildings);
        return new ResponseEntity<>(array.toString(), responseHeaders, httpStatus);
    }

    public ResponseEntity<String> getErrorResponseEntity(Error error, HttpStatus httpStatus) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(error.toString(), responseHeaders, httpStatus);
    }

    public String getRequestBody() {
        try {
            HttpServletRequest request =
                    ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                            .getRequest();
            return request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            return "";
        }
    }

    // TODO
    public boolean isAuthenticated() {
        //Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return true;
    }
}
