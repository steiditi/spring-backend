package com.springboottutorial.service;

import com.springboottutorial.model.Building;
import com.springboottutorial.model.Storey;
import com.springboottutorial.repository.BuildingsRepository;
import com.springboottutorial.repository.StoreysRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InvalidObjectException;
import java.util.Optional;
import java.util.UUID;

@Service
public class StoreyService {

    @Autowired
    private BuildingsRepository buildingsRepository;

    @Autowired
    private StoreysRepository storeysRepository;

    public boolean isCorrectInput(String input) {
        try {
            JSONObject jsonStorey = new JSONObject(input);

            String name = jsonStorey.getString("name");
            String buildingIdString = jsonStorey.getString("building_id");

            if (name.equals("") | buildingIdString.equals("")) {
                throw new InvalidObjectException("");
            }

            return true;
        } catch (JSONException | InvalidObjectException e) {
            return false;
        }
    }

    public boolean storeyHasId(JSONObject storey) {
        try {
            String idString = storey.getString("id");
            UUID id = UUID.fromString(idString);
            return true;
        } catch (JSONException |IllegalArgumentException e) {
            return false;
        }
    }

    public boolean storeyExists(JSONObject jsonStorey) {
        Optional<Storey> optional = storeysRepository.findById(UUID.fromString(jsonStorey.getString("id")));
        return optional.isPresent();
    }
}
