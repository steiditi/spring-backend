package com.springboottutorial.controller;

import com.springboottutorial.model.Error;
import com.springboottutorial.model.Reservation;
import com.springboottutorial.model.Room;
import com.springboottutorial.model.Storey;
import com.springboottutorial.repository.RoomsRepository;
import com.springboottutorial.repository.StoreysRepository;
import com.springboottutorial.service.MainService;
import com.springboottutorial.service.RoomService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/assets/rooms/")
public class RoomsController {

    @Autowired
    private StoreysRepository storeysRepository;

    @Autowired
    private RoomsRepository roomsRepository;

    @Autowired
    private MainService mainService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private Logger logger;

    @GetMapping("/")
    public ResponseEntity<String> getAllRooms(@RequestParam UUID storey_id) {
        Iterable<Room> iter = roomsRepository.findAll();

        List<Room> roomInBuilding = new LinkedList<>();

        for (Room room : iter) {
            if (room.getStoreyId().equals(storey_id)) {
                roomInBuilding.add(room);
            }
        }

        logger.info("API call to [{}]: {} entries found",
                "GET /assets/rooms/",
                roomInBuilding.size());

        return mainService.getBasicResponseEntity(roomInBuilding, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<String> addANewRoom() {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();
            if (roomService.isCorrectInput(body)) {

                JSONObject jsonRoom = new JSONObject(body);

                if (roomService.roomHasId(jsonRoom) && roomService.roomExists(jsonRoom)) {
                    String storeyIdString = jsonRoom.getString("storey_id");
                    UUID storeyId = UUID.fromString(storeyIdString);
                    Optional<Storey> optional = storeysRepository.findById(storeyId);
                    if (optional.isPresent()) {
                        Room room = new Room(jsonRoom);
                        roomsRepository.save(room);

                        logger.info("API call to [{}]: [200] Updated {}",
                                "POST /assets/rooms/",
                                body);

                        return mainService.getBasicResponseEntity(room, HttpStatus.CREATED);
                    } else {
                        logger.info("API call to [{}]: [422] Storey not found with id {}",
                                "POST /assets/rooms/",
                                storeyId);

                        Error error = new Error("Storey not found");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
                    }
                } else {
                    Room room = new Room(jsonRoom);
                    roomsRepository.save(room);

                    logger.info("API call to [{}]: [201] Created {}",
                            "POST /assets/rooms/",
                            body);

                    return mainService.getBasicResponseEntity(room, HttpStatus.CREATED);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "POST /assets/rooms/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "POST /assets/rooms/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{id}/")
    public ResponseEntity<String> getARoomById(@PathVariable UUID id) {
        Optional<Room> room = roomsRepository.findById(id);

        if (room.isPresent()) {
            logger.info("API call to [{}]: [204] Found {}",
                    "GET /assets/rooms/{id}/",
                    new JSONObject(room.get()));

            return mainService.getBasicResponseEntity(room.get(), HttpStatus.OK);
        } else {
            logger.info("API call to [{}]: [404] Could not find room with id {}",
                    "GET /assets/rooms/{id}/",
                    id);

            Error error = new Error("Room not found");
            return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/")
    public ResponseEntity<String> addOrUpdateARoomById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();

            if (roomService.isCorrectInput(body)) {
                JSONObject jsonRoom = new JSONObject(body);
                String idString = jsonRoom.getString("id");
                UUID jsonId = UUID.fromString(idString);

                if (jsonId.equals(id)) {

                    String storeyIdString = jsonRoom.getString("storey_id");
                    UUID storeyId = UUID.fromString(storeyIdString);
                    Optional<Storey> optional = storeysRepository.findById(storeyId);

                    if (optional.isPresent()) {
                        Room room = new Room(jsonRoom);
                        roomsRepository.save(room);

                        logger.info("API call to [{}]: [204] Saved {}",
                                "PUT /assets/rooms/{id}/",
                                body);
                        return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
                    } else {
                        logger.info("API call to [{}]: [422] Storey with id ({}) not found",
                                "PUT /assets/rooms/{id}/",
                                storeyId);

                        Error error = new Error("Storey not found");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    logger.info("API call to [{}]: [422] Mismatching id in url ({}) and object ({})",
                            "PUT /assets/rooms/{id}/",
                            id,
                            jsonId);

                    Error error = new Error("Mismatching id in url and object");
                    return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "PUT /assets/rooms/{id}/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "PUT /assets/rooms/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<String> deleteARoomById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            Optional<Room> room = roomsRepository.findById(id);

            if (room.isPresent()) {
                //check for existing reservations

                Iterable<Reservation> iter;
                try {
                    iter = roomService.getAllReservations();
                } catch (Exception e) {
                    logger.warn("API call to [{}]: [500] Room with id {} could not be deleted: cause is " +
                                    "reservation service is not reachable",
                            "DELETE /assets/rooms/{id}/",
                            id);
                    Error error = new Error("Deletion not possible because of reservation service is not reachable");
                    return mainService.getErrorResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
                }


                for (Reservation reservation : iter) {
                    if (reservation.getRoomId().equals(id)) {
                        logger.info("API call to [{}]: [422] Room with id {} could not be deleted: cause is " +
                                        "existing reservation",
                                "DELETE /assets/storeys/{id}/",
                                id);
                        Error error = new Error("Deletion not possible because of existing reservation");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                }

                roomsRepository.delete(room.get());

                logger.info("API call to [{}]: [204] Deleted room with id {}",
                        "DELETE /assets/rooms/{id}/",
                        id);

                return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                logger.info("API call to [{}]: [404] Room with id {} not found",
                        "DELETE /assets/rooms/{id}/",
                        id);

                Error error = new Error("Room could not be found");
                return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "POST /assets/storeys/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

}
