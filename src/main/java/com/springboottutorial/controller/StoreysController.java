package com.springboottutorial.controller;

import com.springboottutorial.model.Building;
import com.springboottutorial.model.Error;
import com.springboottutorial.model.Room;
import com.springboottutorial.model.Storey;
import com.springboottutorial.repository.BuildingsRepository;
import com.springboottutorial.repository.RoomsRepository;
import com.springboottutorial.repository.StoreysRepository;
import com.springboottutorial.service.MainService;
import com.springboottutorial.service.StoreyService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/assets/storeys/")
public class StoreysController {

    @Autowired
    private BuildingsRepository buildingsRepository;

    @Autowired
    private StoreysRepository storeysRepository;

    @Autowired
    private RoomsRepository roomsRepository;

    @Autowired
    private MainService mainService;

    @Autowired
    private StoreyService storeyService;

    @Autowired
    private Logger logger;

    @GetMapping("/")
    public ResponseEntity<String> getAllStoreys(@RequestParam UUID building_id) {
        Iterable<Storey> iter = storeysRepository.findAll();

        List<Storey> storeyInBuilding = new LinkedList<>();

        for (Storey storey : iter) {
            if (storey.getBuildingId().equals(building_id)) {
                storeyInBuilding.add(storey);
            }
        }

        logger.info("API call to [{}]: {} entries found",
                "GET /assets/storeys/",
                storeyInBuilding.size());

        return mainService.getBasicResponseEntity(storeyInBuilding, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<String> addANewStorey() {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();
            if (storeyService.isCorrectInput(body)) {

                JSONObject jsonStorey = new JSONObject(body);

                if (storeyService.storeyHasId(jsonStorey) && storeyService.storeyExists(jsonStorey)) {
                    String buildingIdString = jsonStorey.getString("building_id");
                    UUID buildingId = UUID.fromString(buildingIdString);
                    Optional<Building> optional = buildingsRepository.findById(buildingId);
                    if (optional.isPresent()) {
                        Storey storey = new Storey(jsonStorey);
                        storeysRepository.save(storey);

                        logger.info("API call to [{}]: [200] Updated {}",
                                "POST /assets/storeys/",
                                body);

                        return mainService.getBasicResponseEntity(storey, HttpStatus.CREATED);
                    } else {
                        logger.info("API call to [{}]: [422] Building not found with id {}",
                                "POST /assets/storeys/",
                                buildingId);

                        Error error = new Error("Building not found");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
                    }
                } else {
                    Storey storey = new Storey(jsonStorey);
                    storeysRepository.save(storey);

                    logger.info("API call to [{}]: [201] Created {}",
                            "POST /assets/storeys/",
                            body);

                    return mainService.getBasicResponseEntity(storey, HttpStatus.CREATED);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "POST /assets/storeys/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "POST /assets/storeys/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{id}/")
    public ResponseEntity<String> getAStoreyById(@PathVariable UUID id) {
        Optional<Storey> storey = storeysRepository.findById(id);

        if (storey.isPresent()) {
            logger.info("API call to [{}]: [204] Found {}",
                    "GET /assets/storeys/{id}/",
                    new JSONObject(storey.get()));

            return mainService.getBasicResponseEntity(storey.get(), HttpStatus.OK);
        } else {
            logger.info("API call to [{}]: [404] Could not find storey with id {}",
                    "GET /assets/storeys/{id}/",
                    id);

            Error error = new Error("Storey not found");
            return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/")
    public ResponseEntity<String> addOrUpdateAStoreyById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();

            if (storeyService.isCorrectInput(body)) {
                JSONObject jsonStorey = new JSONObject(body);
                String idString = jsonStorey.getString("id");
                UUID jsonId = UUID.fromString(idString);

                if (jsonId.equals(id)) {
                    String buildingIdString = jsonStorey.getString("building_id");
                    UUID buildingId = UUID.fromString(buildingIdString);
                    Optional<Building> optional = buildingsRepository.findById(buildingId);

                    if (optional.isPresent()) {
                        Storey storey = new Storey(jsonStorey);
                        storeysRepository.save(storey);

                        logger.info("API call to [{}]: [204] Saved {}",
                                "PUT /assets/storeys/{id}/",
                                body);
                        return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
                    } else {
                        logger.info("API call to [{}]: [422] Building with id ({}) not found",
                                "PUT /assets/storeys/{id}/",
                                buildingId);

                        Error error = new Error("Building not found");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    logger.info("API call to [{}]: [422] Mismatching id in url ({}) and object ({})",
                            "PUT /assets/storeys/{id}/",
                            id,
                            jsonId);

                    Error error = new Error("Mismatching id in url and object");
                    return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "PUT /assets/storeys/{id}/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "PUT /assets/storeys/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<String> deleteAStoreyById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            Optional<Storey> storey = storeysRepository.findById(id);

            if (storey.isPresent()) {
                //check for existing rooms
                Iterable<Room> iter = roomsRepository.findAll();
                for (Room room : iter) {
                    if (room.getStoreyId().equals(id)) {
                        logger.info("API call to [{}]: [422] Storey with id {} could not be deleted: cause is " +
                                        "existing room",
                                "DELETE /assets/storeys/{id}/",
                                id);
                        Error error = new Error("Deletion not possible because of existing room");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                }

                storeysRepository.delete(storey.get());

                logger.info("API call to [{}]: [204] Deleted storey with id {}",
                        "DELETE /assets/storeys/{id}/",
                        id);

                return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                logger.info("API call to [{}]: [404] Storey with id {} not found",
                        "DELETE /assets/storeys/{id}/",
                        id);

                Error error = new Error("Storey could not be found");
                return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "POST /assets/storeys/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }
}
