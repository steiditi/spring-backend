package com.springboottutorial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/**");
//    }


//    @Override
//    protected  void configure(HttpSecurity http) throws Exception {
//        http.cors()
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/api/token").permitAll()
//                .anyRequest()
//                .authenticated()
//                .and()
//                .oauth2ResourceServer()
//                .jwt();
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .httpBasic()
                .and()
                .authorizeRequests(authz -> authz
                        .antMatchers(HttpMethod.GET,"/**").hasAuthority("USER")
                        .antMatchers(HttpMethod.POST, "/**").hasAuthority("USER")
                        .antMatchers(HttpMethod.PUT, "/**").hasAuthority("USER")
                        .antMatchers(HttpMethod.DELETE, "/**").hasAuthority("USER")
                        .anyRequest().authenticated())
                .oauth2ResourceServer(oauth2 -> oauth2.jwt());
    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .httpBasic()
//                .and()
//                .oauth2Login()
//                .and()
//                .logout()
//                .addLogoutHandler(new KeycloakLogoutHandler(new RestTemplate()))
//                .logoutUrl("/logout")
//                .logoutSuccessUrl("/")
//                .and()
//                .authorizeRequests()
//                //still needed for errorpage
//                .antMatchers("/css/**", "/webjars/**", "/img/**",
//                        "/actuator/**", "/js/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/**").hasAnyAuthority("USER")
//                .antMatchers(HttpMethod.PUT, "/**").hasAnyAuthority("USER")
//                .antMatchers(HttpMethod.DELETE, "/**").hasAnyAuthority("USER");
//    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.httpBasic().disable();
//        http.cors().and().csrf().disable();
//    }

    @Bean
    @Scope("prototype")
    public Logger produceLogger(InjectionPoint injectionPoint) {
        Class<?> classOnWired = injectionPoint.getMember().getDeclaringClass();
        return LoggerFactory.getLogger(classOnWired);
    }
}
