package com.springboottutorial.repository;

import com.springboottutorial.model.Building;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.UUID;


@Repository
public interface BuildingsRepository extends CrudRepository<Building, UUID> {

    @Override
    LinkedList<Building> findAll();

}
