package com.springboottutorial.repository;

import com.springboottutorial.model.Reservation;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface ReservationsRepository extends CrudRepository<Reservation, UUID> {

}
