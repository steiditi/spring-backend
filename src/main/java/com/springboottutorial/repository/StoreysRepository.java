package com.springboottutorial.repository;

import com.springboottutorial.model.Storey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface StoreysRepository extends CrudRepository<Storey, UUID> {

}
