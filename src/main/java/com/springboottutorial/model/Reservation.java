package com.springboottutorial.model;

import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

//@Entity
//@Table(name="reservations")
public class Reservation {

//    @Id
    UUID id;

    String from;
    String to;
    UUID roomId;

    public Reservation() {

    }

    public Reservation(JSONObject json) {
        id = UUID.fromString(json.getString("id"));
        from = json.getString("from");
        to = json.getString("to");
        roomId = UUID.fromString(json.getString("room_id"));
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }
}
