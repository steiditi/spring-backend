package com.springboottutorial.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="storeys")
public class Storey {

    @Id
    UUID id;

    UUID buildingId;
    String name;

    public Storey() {

    }

    public Storey(JSONObject json) {
        name = json.getString("name");
        String buildingIdString = json.getString("address");
        buildingId = UUID.fromString(buildingIdString);

        try {
            id = UUID.fromString(json.getString("id"));
        } catch (JSONException e) {
            id = UUID.randomUUID();
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(UUID buildingId) {
        this.buildingId = buildingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
