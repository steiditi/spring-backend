package com.springboottutorial.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="rooms")
public class Room {

    @Id
    UUID id;

    UUID storeyId;
    String name;

    public Room() {

    }

    public Room(JSONObject json) {
        name = json.getString("name");
        String storeyIdString = json.getString("address");
        storeyId = UUID.fromString(storeyIdString);

        try {
            id = UUID.fromString(json.getString("id"));
        } catch (JSONException e) {
            id = UUID.randomUUID();
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getStoreyId() {
        return storeyId;
    }

    public void setStoreyId(UUID storeyId) {
        this.storeyId = storeyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
